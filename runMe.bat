cd C:\Zabbix
powershell write-host -fore Cyan "The Zabbix Agent service will be installed."
PAUSE
.\bin\win64\zabbix_agentd.exe -c .\conf\zabbix_agentd.win.conf -i
powershell write-host -fore Cyan "A firewall role will be added."
PAUSE
Netsh.exe advfirewall firewall add rule name="Zabbix" program="%SystemDrive%\Zabbix\bin\win64\zabbix_agentd.exe" protocol=tcp localport=10050 dir=in enable=yes action=allow profile=public,private,domain
powershell write-host -fore Cyan "Hostname in the configuration file will be edited."
PAUSE
powershell -Command "(gc .\conf\zabbix_agentd.win.conf) -replace 'vmname', $env:computername | Out-File .\conf\zabbix_agentd.win.conf"
powershell write-host -fore Cyan "Encoding of the configuration file will be changed to UTF8."
PAUSE
powershell -Command "$MyPath='.\conf\zabbix_agentd.win.conf'; $MyFile=Get-Content $MyPath; $Utf8NoBomEncoding=New-Object System.Text.UTF8Encoding $False; [System.IO.File]::WriteAllLines($MyPath, $MyFile)"
powershell write-host -fore Cyan "The Zabbix Agent service will be started."
PAUSE
net start "Zabbix Agent"
PAUSE